import {
  INCREMENT_GOOD,
  DECREMENT_GOOD,
  ADD_GOOD,
  REMOVE_GOOD
} from './types';

export const incrementCnt = (value) => {
  return {
    type: INCREMENT_GOOD,
    payload: value
  };
};

export const decrementCnt = (value) => {
  return {
    type: DECREMENT_GOOD,
    payload: value
  };
};

export const addGood = (value) => {
  return (dispatch) => {
    dispatch({
      type: ADD_GOOD,
      payload: value
    });
  };
};

export const removeGood = (value) => {
  return (dispatch) => {
    dispatch({
      type: REMOVE_GOOD,
      payload: value
    });
  };
};