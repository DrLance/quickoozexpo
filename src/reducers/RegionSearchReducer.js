import {
  SEARCH_POSTALCODE
} from '../actions/types';

const INITIAL_STATE = {
  postalcode: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SEARCH_POSTALCODE:
      return { ...state, loading: true, error: '' };
    default:
      return state;
  }
};
