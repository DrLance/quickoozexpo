import React, { Component } from 'react';
import { Scene, Router } from 'react-native-router-flux';
import { SaveButtonNav, CancelButtonNav } from './components/commons/navbar';
import Home from './components/Home/Home';
import SearchRegion from './components/Home/SearchRegion';
import StoreList from './components/Store/StoreList';
import MenuDrawer from './components/Drawer/MenuDrawer';
import Account from './components/user/Account';
import OrderHistory from './components/user/OrderHistory';
import ContactInfo from './components/user/ContactInfo';
import GoodsList from './components/GoodsList';
import GoodCart from './components/GoodCart';
import GoodsPromoView from './components/GoodsPromoView';
import Cart from './components/Cart';
import CartCheckoutForm from './components/CartCheckoutForm';
import Search from './components/Search/Search';
import Confirm from './components/Confirm';

class RouterComponent extends Component {
  render() {
    return (
      <Router key="mainRoute" sceneStyle={styles.mainSceneStyle}>
        <Router key="auth">
          <Scene key="login" component={Home} hideNavBar />
          <Scene key="region" component={SearchRegion} hideNavBar />
        </Router>
        <Router key="drawer" component={MenuDrawer} open={false}>
          <Scene key="main" hideNavBar>
            <Scene key="listStore" component={StoreList} title="Store List" />
            <Scene key="listGoods" component={GoodsList} title="Goods List" />
            <Scene key="goodCart" component={GoodCart} />
            <Scene key="goodsPromo" component={GoodsPromoView} />
            <Scene
              key="account"
              component={Account}
              title="Account detail"
              renderRightButton={SaveButtonNav}
              renderBackButton={CancelButtonNav}
            />
            <Scene key="contact" component={ContactInfo} title="Contacts" />
            <Scene key="orderhistory" component={OrderHistory} title="Order History" />
            <Scene key="cart" component={Cart} title="Cart" />
            <Scene key="cartCheckoutForm" component={CartCheckoutForm} title="Cart" />
            <Scene key="searchForm" component={Search} title="Search" />
            <Scene key="confirm" component={Confirm} title="Confirm Order" />
          </Scene>
        </Router>
      </Router>
    );
  }
}

const styles = {
  mainSceneStyle: {
    backgroundColor: '#fff'
  }
};

export default RouterComponent;
