export default dataGoods = [
  {
    "id": "1",
    "category": "Category 00",
    "picture": "http://lorempixel.com/200/150/food/",
    "goods": [
      {
        "name": "banana 00",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "437.17",
        "rs": 510.58,
        "md5": "589a574553250be33f3b1170624ad2d1",
        "weight": 917.84
      },
      {
        "name": "banana 10",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "321.42",
        "rs": 562.22,
        "md5": "589a574553250be33f3b1170624ad2d13",
        "weight": 900.5
      },
      {
        "name": "banana 20",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "590.47",
        "rs": 283.34,
        "md5": "589a574553250be33f3b1170624ad2d3",
        "weight": 920.28
      },
      {
        "name": "banana 30",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "700.58",
        "rs": 666.66,
        "md5": "589a574553250be33f3b1170624ad2d4",
        "weight": 443.82
      },
      {
        "name": "banana 40",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "235.07",
        "rs": 650.89,
        "md5": "589a574553250be33f3b1170624ad2d55",
        "weight": 931.67
      },
      {
        "name": "banana 50",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "232.42",
        "rs": 644.94,
        "md5": "589a574553250be33f3b1170624ad2d5",
        "weight": 220.39
      },
      {
        "name": "banana 60",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "303.74",
        "rs": 197.11,
        "md5": "589a574553250be33f3b1170624ad2d6",
        "weight": 981.68
      },
      {
        "name": "banana 70",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "233.83",
        "rs": 146.12,
        "md5": "589a574553250be33f3b1170624ad2d7",
        "weight": 6.23
      }
    ]
  },
  {
    "id": "2",
    "category": "Category 10",
    "picture": "http://lorempixel.com/200/150/food/",
    "goods": [
      {
        "name": "banana 00",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "868.26",
        "rs": 140.18,
        "md5": "589a574553250be33f3b1170624ad2d8",
        "weight": 652.93
      },
      {
        "name": "banana 10",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "616.61",
        "rs": 31.12,
        "md5": "589a574553250be33f3b1170624ad2d9",
        "weight": 871.82
      },
      {
        "name": "banana 20",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "502.73",
        "rs": 600.53,
        "md5": "589a574553250be33f3b1170624ad2d10",
        "weight": 237.53
      },
      {
        "name": "banana 30",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "477.59",
        "rs": 321.86,
        "md5": "589a574553250be33f3b1170624ad2d11",
        "weight": 636.85
      },
      {
        "name": "banana 40",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "550.67",
        "rs": 98.28,
        "md5": "589a574553250be33f3b1170624ad2d12",
        "weight": 679.36
      },
      {
        "name": "banana 50",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "550.67",
        "rs": 98.28,
        "md5": "589a574553250be33f3b1170624ad2d22",
        "weight": 679.36
      },
      {
        "name": "banana 60",
        "picture": "http://lorempixel.com/128/128/food/",
        "price": "550.67",
        "rs": 98.28,
        "md5": "589a574553250be33f3b1170624ad2d32",
        "weight": 679.36
      }
    ]
  }
];
