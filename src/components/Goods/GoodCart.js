import React, { Component } from 'react';
import { Image, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Text,
  Footer,
  Body,
  Left,
  Right,
  Header,
  Button,
  Icon
} from 'native-base';
import GoodItem from './commons/GoodItem';
import { incrementCnt, decrementCnt, addGood } from '../actions';

class GoodCart extends Component {
  constructor() {
    super();
    this.state = {
      countNumber: 1
    };
  }

  onPressMin() {
    let cntNum = this.state.countNumber;
    cntNum = cntNum <= 1 ? 1 : cntNum - 1;
    this.state.countNumber = cntNum;
    this.props.decrementCnt(cntNum);
  }

  onPressPls() {
    this.state.countNumber++;
    this.props.incrementCnt(this.state.countNumber);
  }

  onPressBack() {
    Actions.pop();
  }

  onAdd() {
    for (let i = 0; i < this.state.countNumber; i++) {
      this.props.goodChoice.push({
        id: this.props.id,
        name: this.props.name,
        price: this.props.price,
        cnt_id: i,
        picture: this.props.picture,
        md5: this.props.md5
      });
    }
    this.props.addGood(this.props.goodChoice);
    this.setState({ countNumber: 1 });
    Actions.listGoods({ type: 'BackAction' });
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#fff' }}>
          <Left>
            <Button transparent onPress={this.onPressBack.bind(this)}>
              <Icon style={{ color: '#000' }} name="md-arrow-back" />
            </Button>
          </Left>
          <Right>
            <Button transparent>
              <Icon style={{ color: '#000', fontSize: 35 }} name="md-more" />
            </Button>
          </Right>
        </Header>
        <Content style={{ paddingTop: 5 }}>
          <Body>
            <Image
              source={{ uri: 'http://lorempixel.com/200/150/food/' }}
              style={styles.imageStyle}
            />
            <Text>{this.props.rs}</Text>
            <Text>{this.props.rs}</Text>
            <Text
              style={{
                color: '#fff',
                backgroundColor: 'red',
                borderRadius: 8,
                padding: 2,
                alignSelf: 'center'
              }}
            >
              {this.props.rs}
            </Text>
            <Text style={{ color: '#000', fontSize: 15 }}>
              {this.props.name}
            </Text>
            <Text style={styles.fontTxtSz}>
              {this.props.weight}
            </Text>
          </Body>
          <Text style={styles.productStyle}>Related Products</Text>
          <Left>
            <FlatList
              horizontal
              data={this.props.goodsList[0].goods}
              renderItem={({ item }) => <GoodItem {...item} />}
              keyExtractor={item => item.name}
            />
          </Left>
        </Content>
        <Footer style={{ alignItems: 'center', backgroundColor: '#fff' }}>
          <Body>
            <Button transparent onPress={this.onPressMin.bind(this)}>
              <Icon style={{ color: '#000' }} name="md-remove" />
            </Button>
            <Text style={{ marginLeft: 5, marginRight: 5 }}>
              {this.state.countNumber}
            </Text>
            <Button transparent onPress={this.onPressPls.bind(this)}>
              <Icon style={{ color: '#000' }} name="md-add" />
            </Button>
            <Right>
              <Button transparent onPress={this.onAdd.bind(this)}>
                <Text style={styles.addCartStyle}>ADD TO CART</Text>
              </Button>
            </Right>
          </Body>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    goodsList: state.goodsList,
    countGood: state.goodCart.countGood,
    goodChoice: state.goodChoice,
    goodCartChoice: state.goodCart.goodChoice
  };
};

const styles = {
  containerStyleList: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    justifyContent: 'space-around',
    padding: 5,
    borderColor: '#F7F7F7',
    borderRightWidth: 4,
    borderBottomWidth: 4,
    elevation: 4
  },
  imageStyle: {
    height: 200,
    width: 150
  },
  imageStyleList: {
    height: 100,
    width: 75
  },
  fontTxtStyle: {
    fontWeight: 'bold'
  },
  productStyle: {
    paddingLeft: 10,
    backgroundColor: '#F7F6F4',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    height: 25
  },
  textStyle: {
    fontSize: 10,
    fontWeight: 'bold',
    color: '#fff',
    position: 'absolute',
    top: 10,
    left: 18,
    right: 0
  },
  addCartStyle: {
    color: '#000'
  }
};

export default connect(mapStateToProps, {
  incrementCnt,
  decrementCnt,
  addGood
})(GoodCart);
