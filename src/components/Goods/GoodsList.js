import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Container, Content } from 'native-base';
import { CatHeader, StoreGoods, CatGoodsList, MainHeader } from './commons/';
import { openDrawer } from '../actions/DrawerActions';

class GoodsList extends Component {
  constructor() {
    super();
    this.state = {
      isCart: false,
      countCart: 0
    };
  }

  onShopPress() {
    this.setState({ isCart: 0, countCart: this.props.goodChoice.length });
  }

  onCatPress() {
    this.setState({ isCart: 1, countCart: this.props.goodChoice.length });
  }

  onPromoPress() {
    Actions.goodsPromo();
  }

  onPressMenu() {
    this.props.openDrawer();
  }

  renderCatList() {
    if (this.state.isCart) {
      return (
        <CatGoodsList
          dataSource={this.props.goodsList}
          onPromoPress={this.onPromoPress.bind(this)}
          name={this.props.name}
        />
      );
    }
    return <StoreGoods dataSource={this.props.goodsList} />;
  }

  render() {
    return (
      <Container>
        <MainHeader
          cartLength={this.props.lengthChoice}
          onPressMenu={this.onPressMenu.bind(this)}
        />
        <Content>
          <CatHeader
            name={this.props.title}
            onCat={this.onCatPress.bind(this)}
            onShop={this.onShopPress.bind(this)}
          />
          {this.renderCatList()}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    goodsList: state.goodsList,
    lengthChoice: state.goodCart.goodChoice.length,
    goodChoice: state.goodChoice,
    goodCartChoice: state.goodCart.goodChoice,
    goodsCategoryList: state.goodsCategoryList
  };
};

export default connect(mapStateToProps, { openDrawer })(GoodsList);
