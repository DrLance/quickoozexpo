import React from 'react';
import { Actions } from 'react-native-router-flux';
import {
  Header,
  Button,
  Left,
  Icon
} from 'native-base';

const BackHeader = () => {
  return (
    <Header style={{ backgroundColor: '#fff' }}>
      <Left>
        <Button transparent onPress={() => { Actions.pop(); }}>
          <Icon style={{ color: '#000' }} name='md-arrow-back' />
        </Button>
      </Left>
    </Header>
  );
};

export { BackHeader };
