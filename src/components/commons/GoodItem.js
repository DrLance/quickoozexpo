import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';

class GoodItem extends Component {

  onPressRow() {
    Actions.goodCart(this.props);
  }

  render() {
    return (
      <View style={styles.containerStyle}>
        <TouchableOpacity onPress={this.onPressRow.bind(this)}>
          <View style={{ alignItems: 'center', margin: 20 }}>
            <Image source={{ uri: this.props.picture }} style={styles.imageStyle} />
          </View>
          <View style={{ paddingLeft: 10 }}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={[{ color: 'red' }, styles.fontTxtSz]}>{this.props.price}</Text>
              <Text style={[{ paddingLeft: 5 }, styles.fontTxtSz]}>{this.props.price}</Text>
            </View>
            <Text
              style={[{
                color: '#fff',
                backgroundColor: 'red',
                borderRadius: 25,
                padding: 2,
                alignSelf: 'flex-start'
              }, styles.fontTxtSz]}
            >
              {this.props.rs}
            </Text>
            <Text style={[{ color: '#000', fontSize: 12 }]}>
              {this.props.name}
            </Text>
            <Text style={styles.fontTxtSz}>
              {this.props.weight}
            </Text>
          </View>
        </TouchableOpacity>
      </View>

    );
  }
}

const styles = {
  containerStyle: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    justifyContent: 'space-around',
    padding: 5,
    borderColor: '#F7F7F7',
    borderRightWidth: 4,
    elevation: 4
  },
  imageStyle: {
    height: 100,
    width: 75
  },
  productStyle: {
    backgroundColor: '#F7F6F4',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: ('100%'),
    height: 50
  },
  fontTxtStyle: {
    fontWeight: 'bold'
  },
  fontTxtSz: {
    fontSize: 10,
    fontWeight: 'bold'
  }
};

export default GoodItem;
