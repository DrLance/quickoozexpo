import React, { Component } from 'react';
import { View, Text, FlatList, TouchableWithoutFeedback, Image } from 'react-native';
import { Icon } from 'native-base';
import Collapsible from 'react-native-collapsible';

class GoodsCategoryItem extends Component {
  constructor(props) {
    super(props);
    this.state = { isCollapsed: true, iconArrow: 'ios-arrow-forward' };
  }

  onPressArrow() {
    this.setState({ isCollapsed: !this.state.isCollapsed });

    if (this.state.isCollapsed) {
      this.setState({ iconArrow: 'ios-arrow-down' });
    } else {
      this.setState({ iconArrow: 'ios-arrow-forward' });
    }
  }
  render() {
    return (
      <View style={styles.containerStyle}>
        <View style={styles.categoryListStyle}>
          <Image source={{ uri: this.props.picture }} style={styles.imageStyle} />
          <Text
            onPress={this.onPressArrow.bind(this)}
            style={styles.textStyle}
          >
            {this.props.category}

          </Text>
          <View style={styles.arrowStyle}>
            <Icon
              name={this.state.iconArrow}
              size={20}
              color={'#000'}
              onPress={this.onPressArrow.bind(this)}
            />
          </View>
        </View>
        <Collapsible collapsed={this.state.isCollapsed}>
          <FlatList
            data={this.props.goods}
            renderItem={({ item }) => (
              <TouchableWithoutFeedback>
                <View style={styles.listStyle}>
                  <View style={{ alignItems: 'center' }}>
                    <Image source={{ uri: item.picture }} style={styles.imageStyle} />
                  </View>
                  <Text style={styles.textStyle}>
                    {item.name}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            )}
            keyExtractor={item => item.name}
          //ItemSeparatorComponent={this.renderSeparator}
          //ListHeaderComponent={this.renderHeader}
          //ListFooterComponent={<Footer />}
          //onRefresh={this.handleRefresh}
          //refreshing={this.state.refreshing}
          //onEndReached={this.handleLoadMore}
          //onEndReachedThreshold={50}
          />
        </Collapsible>
      </View >
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 10,
  },
  textStyle: {
    fontSize: 15,
    color: '#000',
    left: 10,
  },
  categoryListStyle: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  imageStyle: {
    height: 20,
    width: 20
  },
  listStyle: {
    flexDirection: 'row',
    left: 2,
    justifyContent: 'flex-start',
  },
  arrowStyle: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    right: 10
  }
};

export default GoodsCategoryItem;
