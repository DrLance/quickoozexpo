import React from 'react';
import { Image, FlatList } from 'react-native';
import {
  Text,
  Body,
  Button,
  View,
  Icon as IconNB
} from 'native-base';
import GoodsItemList from './GoodsItemList';

const StoreGoods = (props) => {
  return (
    <View>
      <Body style={styles.logoStyle}>
        <Image
          source={{ uri: 'http://lorempixel.com/300/175/food/' }}
          style={{ height: 80, width: 250, bottom: 5 }}
        />
        <Button
          iconLeft
          rounded
          small
          style={{ backgroundColor: '#fff', alignSelf: 'center' }}
        >
          <IconNB
            style={{ color: 'green', fontSize: 22, left: -20 }}
            name='ios-checkmark-circle-outline'
          />
          <Text style={{ color: 'green' }}>prices are same as store</Text>
        </Button>
        <Button
          iconLeft
          rounded
          small
          style={{ backgroundColor: '#fff', alignSelf: 'center', margin: 5 }}
        >
          <IconNB
            style={{ color: '#0b868d', fontSize: 22, left: -20 }}
            name='clock'
          />
          <Text style={{ color: '#0b868d' }}>Open from 10:00 AM to 9:00 PM</Text>
        </Button>
      </Body>
      <Body>
        <FlatList
          data={props.dataSource}
          renderItem={({ item }) => (<GoodsItemList {...item} />)}
          keyExtractor={item => item.id}
        />
      </Body>
    </View>
  );
};

const styles = {
  headerStyle: {
    backgroundColor: '#fff',
    alignItems: 'center',
    height: 75,
  },
  logoStyle: {
    height: 175,
    backgroundColor: '#FCD403',
    width: ('100%'),
    justifyContent: 'center'
  }
};

export { StoreGoods };
