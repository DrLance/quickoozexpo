import React, { Component } from 'react';
import { View, Text, ListView, TouchableWithoutFeedback } from 'react-native';
import GoodItem from './GoodItem';

class GoodsItemList extends Component {

  componentWillMount() {
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.dataSource = ds.cloneWithRows(this.props.goods);
  }

  render() {
    return (
      <TouchableWithoutFeedback>
        <View style={{ flexDirection: 'column' }}>
          <View style={styles.productStyle}>
            <View style={{ flex: 4 }}>
              <Text style={{ left: ('10%') }}>{this.props.category}</Text>
            </View>
            <View style={{ flex: 2, alignItems: 'center' }}>
              <Text
                style={{
                  borderWidth: 1,
                  padding: 2,
                  fontSize: 10,
                  color: '#0b868d',
                  borderColor: '#0b868d',
                  alignSelf: 'center'
                }}
              >View more</Text>
            </View>
          </View>
          <ListView
            horizontal
            dataSource={this.dataSource}
            renderRow={(item) => <GoodItem {...item} />}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  containerStyle: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    justifyContent: 'space-around',
    padding: 5,
    borderColor: '#F7F7F7',
    borderRightWidth: 4,
    elevation: 4
  },
  imageStyle: {
    height: 100,
    width: 75
  },
  productStyle: {
    backgroundColor: '#F7F6F4',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: ('100%'),
    height: 50
  },
  fontTxtStyle: {
    fontWeight: 'bold'
  },
  fontTxtSz: {
    fontSize: 10,
    fontWeight: 'bold'
  }
};

export default GoodsItemList;
