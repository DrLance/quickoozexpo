import React from 'react';
import { Actions } from 'react-native-router-flux';
import { Text, Header, Button, Left, Right, Body, Icon } from 'native-base';

const MainHeader = props => {
  return (
    <Header searchBar rounded style={{ backgroundColor: '#fff' }}>
      <Left>
        <Button transparent onPress={props.onPressMenu}>
          <Icon style={{ color: '#000' }} name="menu" />
        </Button>
      </Left>
      <Body>
        <Text>Quickooz</Text>
      </Body>
      <Right>
        <Button
          transparent
          onPress={() => {
            Actions.searchForm();
          }}
        >
          <Icon style={{ color: '#000', fontSize: 35 }} name="md-search" />
        </Button>
        <Button
          transparent
          onPress={() => {
            Actions.cart();
          }}
        >
          <Icon style={{ color: '#000', fontSize: 35 }} name="md-cart" />
          <Text style={styles.textStyle}>{props.cartLength}</Text>
        </Button>
      </Right>
    </Header>
  );
};

const styles = {
  textStyle: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#fff',
    position: 'absolute',
    top: 7,
    left: 26,
    right: 0
  }
};

export { MainHeader };
