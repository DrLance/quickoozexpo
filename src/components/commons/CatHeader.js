import React from 'react';
import {
  Text,
  Header,
  Button,
  Grid,
  Col,
  Icon as IconNB
} from 'native-base';

const CatHeader = (props) => {
  return (
    <Header style={styles.headerStyle}>
      <Grid>
        <Col style={styles.columnStyle}>
          <Text style={styles.filterTextStyle}>Shopes in {props.name}</Text>
          <Text style={styles.filterTextStyle}>Laugfs Supermarket</Text>
        </Col>
        <Col style={styles.columnStyleArrow}>
          <Button transparent onPress={props.onShop}>
            <IconNB
              style={{ color: '#0b868d', fontSize: 25 }}
              name='ios-arrow-dropdown-outline'
            />
          </Button>
        </Col>
        <Col style={styles.columnStyle}>
          <Text style={styles.filterTextStyle}>Category</Text>
          <Text style={styles.filterTextStyle}>SELECT Category</Text>
        </Col>
        <Col style={styles.columnStyleArrow}>
          <Button transparent onPress={props.onCat}>
            <IconNB
              style={{ color: '#0b868d', fontSize: 25 }}
              name='ios-arrow-dropdown-outline'
            />
          </Button>
        </Col>
      </Grid>
    </Header>
  );
};

const styles = {
  headerStyle: {
    backgroundColor: '#fff',
    alignItems: 'center',
    height: 75,
  },
  columnStyle: {
    justifyContent: 'center',
  },
  filterTextStyle: {
    fontSize: 12,
    alignItems: 'flex-start',
    color: '#0b868d',
  },
  columnStyleArrow: {
    justifyContent: 'center',
    width: ('20%'),
    marginLeft: -20,
    left: 15

  }
};

export { CatHeader };
