import React, { Component } from 'react';
import { Image, Modal } from 'react-native';
import { connect } from 'react-redux';
import { _ } from 'lodash';
import { Left, Right, Text, View, Body, Button, Icon } from 'native-base';
import { addGood, refreshPrice, removeGood } from '../../actions';

class CartItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      txtButton: 'IN CART',
      disabledBtn: true,
      countNumber: 0,
      isRemove: false
    };
    this.state.countNumber = parseInt(this.props.cnt, 10);
  }
  onPressRow() {
    this.setState({ modalVisible: true });
  }

  onPressMin() {
    let cntNum = parseInt(this.state.countNumber, 10);
    cntNum = cntNum === 0 ? cntNum : cntNum - 1;
    if (cntNum === 0) {
      this.setState({
        txtButton: 'REMOVE',
        disabledBtn: true,
        countNumber: cntNum
      });
    } else {
      this.setState({
        txtButton: 'ADD',
        disabledBtn: false,
        countNumber: cntNum
      });
    }
  }

  onPressPls() {
    let cntNum = parseInt(this.state.countNumber, 10);
    cntNum++;
    this.setState({ countNumber: cntNum, disabledBtn: false, txtButton: 'ADD' });
  }

  onEdit() {
    const tmpChoice2 = this.props.goodChoice.filter(i => {
      return true;
    });
    for (let i = 0; i < this.state.countNumber; i++) {
      tmpChoice2.push({
        id: i,
        name: this.props.name,
        price: this.props.price,
        cnt_id: i,
        picture: this.props.picture,
        md5: this.props.md5
      });
    }

    this.props.addGood(tmpChoice2);
    this.setState({ modalVisible: false });
  }

  onRemove() {
    let tmpChoice = [];
    tmpChoice = this.props.goodChoice.filter(i => {
      if (i.md5 === this.props.md5) {
        return false;
      }
      return true;
    });
    this.props.removeGood(tmpChoice);
    this.setState({ modalVisible: false });
  }

  render() {
    return (
      <View style={styles.containerStyle}>
        <Left style={{ left: 15 }}>
          <Text>{this.props.cnt}</Text>
        </Left>
        <Body style={{ left: -30 }}>
          <Image
            style={styles.imageStyle}
            source={{
              uri: this.props.picture
            }}
          />
        </Body>
        <Body>
          <Text>{this.props.name}</Text>
        </Body>
        <Right>
          <Text>{this.props.sum_price.toFixed(2)}$</Text>
        </Right>
        <Button transparent onPress={this.onPressRow.bind(this)} style={{ alignSelf: 'center' }}>
          <Icon name="md-create" style={{ color: '#000', fontSize: 15 }} />
        </Button>
        <Modal
          animationType={'slide'}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert('Want to be close? Press the button!');
          }}
        >
          <View padder>
            <View style={styles.modalStyle}>
              <Left>
                <Button
                  transparent
                  onPress={() => {
                    this.setState({ modalVisible: false });
                  }}
                >
                  <Icon active style={{ color: '#000', fontSize: 30 }} name="md-close" />
                </Button>
              </Left>
              <Right>
                <Button transparent onPress={this.onRemove.bind(this)}>
                  <Icon active style={{ color: '#000', fontSize: 30 }} name="md-trash" />
                </Button>
              </Right>
            </View>
            <View style={{ alignItems: 'center' }}>
              <Image source={{ uri: this.props.picture }} style={styles.modalImageStyle} />
              <Text>{this.props.name}</Text>
            </View>
            <View style={styles.modalStyle}>
              <Button transparent onPress={this.onPressMin.bind(this)}>
                <Icon style={{ color: '#000' }} name="md-remove" />
              </Button>
              <Text style={{ marginLeft: 5, marginRight: 5 }}>
                {this.state.countNumber}
              </Text>
              <Button transparent onPress={this.onPressPls.bind(this)}>
                <Icon style={{ color: '#000' }} name="md-add" />
              </Button>
              <Right>
                <Button
                  transparent
                  disabled={this.state.disabledBtn}
                  onPress={this.onEdit.bind(this)}
                  small
                >
                  <Text style={styles.btnPlusStyle}>{this.state.txtButton}</Text>
                </Button>
              </Right>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    goodChoice: state.goodChoice,
    goodCartChoice: state.goodCart.goodChoice
  };
};

const styles = {
  containerStyle: {
    flexDirection: 'row',
    height: 100,
    backgroundColor: '#DFDFDF'
  },
  modalStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    elevation: 2,
    borderRadius: 2,
    padding: 5
  },
  imageStyle: {
    height: 80,
    width: 60
  },
  modalImageStyle: {
    height: 200,
    width: 150
  },
  btnPlusStyle: {
    fontWeight: 'bold',
    color: '#000'
  }
};

export default connect(mapStateToProps, { addGood, refreshPrice, removeGood })(CartItem);
