import React from 'react';
import { TextInput, View, Text, Image } from 'react-native';

const Input = ({
  label,
  value,
  onChangeText,
  placeholder,
  secureTextEntry,
  imgSource
}) => {
  const { inputStyle, labelStyle, containerStyle, imgStyle } = styles;
  return (
    <View style={containerStyle}>
      <Image style={imgStyle} source={imgSource} />
      <Text style={labelStyle}>{label}</Text>
      <TextInput
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        autoCorrect={false}
        style={inputStyle}
        value={value}
        onChangeText={onChangeText}
      />
    </View>
  );
};

const styles = {
  inputStyle: {
    color: '#000',
    paddingRight: 5,
    fontSize: 18,
    lineHeight: 25,
    flex: 2
  },
  labelStyle: {
    fontSize: 18
  },
  containerStyle: {
    height: 40,
    flexDirection: 'row'
  },
  imgStyle: {
    height: 25
  }
};

export { Input };
