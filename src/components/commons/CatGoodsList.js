import React from 'react';
import { FlatList } from 'react-native';
import {
  Text,
  Form,
  Button,
  Icon as IconNB
} from 'native-base';
import GoodsCategoryItem from './GoodsCategoryItem';

const CatGoodsList = (props) => {
  return (
    <Form>
      <Button transparent iconLeft>
        <IconNB name="heart" style={{ color: 'red' }} />
        <Text style={styles.textStyle}>Favourites</Text>
      </Button>
      <Button transparent iconLeft>
        <IconNB name="md-pricetags" style={{ color: 'orange' }} />
        <Text style={styles.textStyle} onPress={props.onPromoPress}>Promo products</Text>
      </Button>
      <Button transparent iconLeft>
        <IconNB name="md-archive" style={{ color: '#FCD403' }} />
        <Text style={styles.textStyle}>Purchase history</Text>
      </Button>
      <FlatList
        data={props.dataSource}
        renderItem={({ item }) => (<GoodsCategoryItem {...item} />)}
        keyExtractor={item => item.id}
      />
    </Form>
  );
};

const styles = {
  containerStyle: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  textStyle: {
    color: '#000'
  }
};

export { CatGoodsList };
