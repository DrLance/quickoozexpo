import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';

class BasketButtonNav extends Component {
  constructor() {
    super();
    this.state = {
      countNumber: 0
    };
  }

  render() {
    return (
      <TouchableOpacity
        style={[
          {
            height: 24,
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center'
          }
        ]}
        onPress={() => {
          Actions.drawer();
        }}
      >
        <View style={{ paddingLeft: 30 }}>
          <Icon.Button size={26} name="shopping-cart" color="#000" backgroundColor="#EFEFF2">
            <Text style={styles.textStyle}>0</Text>
          </Icon.Button>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = {
  textStyle: {
    fontSize: 10,
    fontWeight: 'bold',
    color: '#fff',
    position: 'absolute',
    top: 15,
    left: 18,
    right: 0
  }
};

export default BasketButtonNav;
