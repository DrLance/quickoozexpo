import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';

const CancelButtonNav = () => {
  return (
    <TouchableOpacity
      style={[{
        height: 27,
        width: ('100%'),
        flexDirection: 'row',
        alignItems: 'center',
      }]}
      onPress={() => { Actions.drawer({ type: 'BackAction' }); }}
    >
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Icon
          name="clear"
          size={25}
          color={'#000'}
        />
        <Text>Cancel</Text>
      </View>
    </TouchableOpacity>
  );
};

export { CancelButtonNav };
