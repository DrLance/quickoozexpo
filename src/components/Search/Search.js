import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Container, Body, Header, Left, Button, Icon, Item, Input } from 'native-base';

class Search extends Component {
  render() {
    return (
      <Container>
        <Header searchBar rounded style={{ backgroundColor: '#fff' }}>
          <Left>
            <Button
              transparent
              onPress={() => {
                Actions.pop();
              }}
            >
              <Icon name="md-arrow-round-back" style={{ color: '#000' }} />
            </Button>
          </Left>
          <Body>
            <Item style={{ left: -80, width: '100%' }}>
              <Icon name="ios-search" />
              <Input placeholder="Search" />
            </Item>
          </Body>
        </Header>
      </Container>
    );
  }
}

export default Search;
