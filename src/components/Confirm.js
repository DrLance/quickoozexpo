import React, { Component } from 'react';
import { Container, Grid, Col, Icon, Text, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';

class Confirm extends Component {
  render() {
    return (
      <Container>
        <Grid>
          <Col style={styles.containerStyle}>
            <Icon name="md-checkmark" style={{ fontSize: 40, color: 'green' }} />
            <Text style={{ fontSize: 35, color: 'green' }}>You Order is Sent!</Text>
            <Button
              full
              success
              onPress={() => {
                Actions.pop();
              }}
            >
              <Text>Back</Text>
            </Button>
          </Col>
        </Grid>
      </Container>
    );
  }
}

const styles = {
  containerStyle: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }
};

export default Confirm;
