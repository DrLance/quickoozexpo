import React, { Component } from 'react';
import { Image } from 'react-native';
import { View, Text, Container } from 'native-base';
import { BackHeader } from '../commons';
import images from '../../config/images';

class ContactInfo extends Component {
  render() {
    return (
      <Container>
        <BackHeader />
        <View style={styles.containerStyle}>
          <Image
            style={styles.thumbnailStyle}
            source={images.logo}
          />
          <View style={styles.contactStyle}>
            <View style={styles.contactInfoStyle}>
              <Text style={styles.textStyleStart}>Telephone</Text>
              <Text style={styles.textStyleEnd}>+44 4444 444 44</Text>
            </View>
            <View style={styles.contactInfoStyle}>
              <Text style={styles.textStyleStart}>Email</Text>
              <Text style={styles.textStyleEnd}>test@test.com</Text>
            </View>
          </View>
        </View>
      </Container>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 50,
    alignItems: 'center',
    flexDirection: 'column'
  },
  thumbnailStyle: {
    height: 150,
    width: 150,
    borderRadius: 15
  },
  contactStyle: {
    width: ('90%'),
    flexDirection: 'column',
    borderWidth: 0.2,
    marginTop: 20,
    elevation: 2
  },
  contactInfoStyle: {
    flexDirection: 'row',
    backgroundColor: '#E9E9E9',
  },
  textStyleStart: {
    alignItems: 'flex-start',
    flex: 3,
    padding: 20,
  },
  textStyleEnd: {
    alignItems: 'flex-end',
    flex: 3,
    padding: 20,
  }
};

export default ContactInfo;
