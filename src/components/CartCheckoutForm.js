import React, { Component } from 'react';
import { Modal, Platform } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import {
  Container,
  Content,
  Text,
  Left,
  Body,
  Button,
  Footer,
  Item,
  Icon,
  Picker,
  Card,
  CardItem,
  Right,
  ListItem,
  ActionSheet,
  View,
  Input,
  Form
} from 'native-base';
import { BackHeader } from './commons';

const BUTTONS = [
  'Cash on Delivery'
];
const ADDRESS_BUTTONS = [
  'My Address'
];
const DESTRUCTIVE_INDEX = 1;
const CANCEL_INDEX = 2;

class CartCheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentMethod: 'Payment Method',
      addressValue: 'Address',
      phoneValue: '',
      modalVisible: false,
      selectedItem: undefined,
      selected1: 'key1',
      results: {
        items: [],
      },
    };
  }

  onValueChange(value) {
    this.setState({
      selected1: value,
    });
  }

  render() {
    return (
      <Container containerStyle={{ backgroundColor: '#DFDFDF' }}>
        <BackHeader />
        <Content padder>
          <Card>
            <CardItem header bordered>
              <Text style={{ color: '#888888' }}>Payment Method</Text>
            </CardItem>
            <CardItem>
              <Icon name="card" />
              <Text>{this.state.paymentMethod}</Text>
              <Right>
                <Button
                  transparent
                  onPress={
                    () => ActionSheet.show({
                      options: BUTTONS,
                      cancelButtonIndex: CANCEL_INDEX,
                      destructiveButtonIndex: DESTRUCTIVE_INDEX,
                      title: 'Select Payment Method'
                    },
                      (buttonIndex) => {
                        this.setState({ paymentMethod: BUTTONS[buttonIndex] });
                      }
                    )}
                >
                  <Text>ADD</Text>
                </Button>
              </Right>
            </CardItem>
            <CardItem>
              <Icon name="card" />
              <Text>Coupons1</Text>
              <Right>
                <Button transparent>
                  <Text>ADD</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>

          <Card>
            <CardItem header bordered>
              <Text style={{ color: '#888888' }}>Delivery</Text>
            </CardItem>
            <CardItem>
              <Icon name="md-home" />
              <Text>{this.state.addressValue}</Text>
              <Right>
                <Button
                  transparent
                  onPress={
                    () => ActionSheet.show({
                      options: ADDRESS_BUTTONS,
                      cancelButtonIndex: CANCEL_INDEX,
                      destructiveButtonIndex: DESTRUCTIVE_INDEX,
                      title: 'Select Address'
                    },
                      (buttonIndex) => {
                        this.setState({ addressValue: ADDRESS_BUTTONS[buttonIndex] });
                      }
                    )}
                >
                  <Text>ADD</Text>
                </Button>
              </Right>
            </CardItem>
            <CardItem>
              <Icon name="md-call" />
              <Text placeholder='Phone'>{this.state.phoneValue || 'Phone'}</Text>
              <Right>
                <Button
                  transparent
                  onPress={() => {
                    this.setState({ modalVisible: true });
                  }}
                >
                  <Text>ADD</Text>
                </Button>
              </Right>
            </CardItem>
            <CardItem>
              <Text>Delivery Time</Text>
            </CardItem>
            <CardItem>
              <Text>Delivery Time</Text>
            </CardItem>

          </Card>

          <Card>
            <CardItem header bordered>
              <Text style={{ color: '#888888' }}>Delivery Time</Text>
            </CardItem>
            <CardItem>
              <Icon name="card" />
              <Text>Delivery Time</Text>
              <Right>
                <Picker
                  iosHeader="Select one"
                  mode="dialog"
                  style={{ width: (Platform.OS === 'ios') ? undefined : 120 }}
                  selectedValue={this.state.selected1}
                  onValueChange={this.onValueChange.bind(this)}
                >
                  <Item label="Today" value="key0" />
                  <Item label="Tomorrow" value="key1" />
                  <Item label="Morning" value="key2" />
                </Picker>
              </Right>
            </CardItem>
          </Card>

          <ListItem itemHeader first>
            <Text style={{ fontSize: 20 }}>Order Detail</Text>
          </ListItem>
          <ListItem >
            <Text>Sub Total</Text>
            <Right>
              <Text>{this.props.sumPrice.toFixed(2)}$</Text>
            </Right>
          </ListItem>
          <ListItem >
            <Text>Delivery Charge</Text>
            <Right>
              <Text>0$</Text>
            </Right>
          </ListItem>
          <ListItem >
            <Text>Total</Text>
            <Right>
              <Text>{this.props.sumPrice.toFixed(2)}$</Text>
            </Right>
          </ListItem>
          <ListItem >
            <Text>You Saved</Text>
            <Right>
              <Text>0$</Text>
            </Right>
          </ListItem>


        </Content>
        <Footer style={{ alignItems: 'center', backgroundColor: '#FF7C00' }}>
          <Body style={{ justifyContent: 'center' }}>
            <Button transparent onPress={() => { Actions.confirm(); }}>
              <Text style={{ color: '#fff' }}>CHECKOUT</Text>
            </Button>
          </Body>
        </Footer>
        <Modal
          animationType={'fade'}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => { alert('Modal has been closed.'); }}
        >
          <Container>
            <Content padder>
              <Form>
                <Item floatingLabel last>
                  <Input
                    onChangeText={(value) => {
                      this.setState({ phoneValue: value });
                    }}
                    value={this.state.phoneValue}
                    placeholder='Phone number'
                  />
                </Item>
              </Form>
              <View style={{ flexDirection: 'row', marginTop: 20 }}>
                <Left>
                  <Button
                    full
                    onPress={() => {
                      this.setState({ modalVisible: false });
                    }}
                  >
                    <Text style={{ color: '#fff' }}>Cancel</Text>
                  </Button>
                </Left>
                <Right>
                  <Button
                    full
                    onPress={() => {
                      this.setState({ modalVisible: false });
                    }}
                  >
                    <Text style={{ color: '#fff' }}>Ok</Text>
                  </Button>
                </Right>
              </View>
            </Content>
          </Container>
        </Modal>
      </Container >
    );
  }
}

const mapStateToProps = state => {
  return {
    goodChoice: state.goodCart.goodChoice,
    sumPrice: state.goodCart.sumPrice
  };
};

export default connect(mapStateToProps)(CartCheckoutForm);
