import React, { Component } from 'react';
import { Drawer } from 'native-base';
import { DefaultRenderer } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { closeDrawer } from '../../actions/DrawerActions';
import SideBar from './sidebar';

class MenuDrawer extends Component {
  componentDidUpdate() {
    if (this.props.drawer.drawerState === 'opened') {
      this.drawer._root.open();
    }

    if (this.props.drawer.drawerState === 'closed') {
      this.drawer._root.close();
    }
  }

  closeDrawer() {
    if (this.props.drawerState === 'opened') {
      this.props.closeDrawer();
    }
  }

  render() {
    return (
      <Drawer
        ref={ref => {
          this.drawer = ref;
        }}
        onClose={() => this.closeDrawer()}
        content={<SideBar />}
        tapToClose
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    drawer: state.drawer
  };
};
export default connect(mapStateToProps, { closeDrawer })(MenuDrawer);
