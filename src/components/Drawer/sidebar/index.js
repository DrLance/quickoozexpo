import React, { Component } from 'react';
import { Image } from 'react-native';
import { connect } from 'react-redux';
import { Content, Text, List, ListItem, Icon, Container, Left } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { closeDrawer } from '../../../actions';
import styles from './style';

const drawerImage = require('../../../img/logo.png');

const datas = [
  {
    name: 'Home',
    route: 'listStore',
    icon: 'md-home',
    bg: '#C5F442'
  },
  {
    name: 'Account',
    route: 'account',
    icon: 'md-person',
    bg: '#477EEA',
    types: '8'
  },
  {
    name: 'Order History',
    route: 'orderhistory',
    icon: 'md-archive',
    bg: '#DA4437',
    types: '4'
  },
  {
    name: 'Contact Us',
    route: 'contact',
    icon: 'md-information-circle',
    bg: '#4DCAE0'
  }
];

class SideMenu extends Component {
  render() {
    return (
      <Container>
        <Content bounces={false} style={{ flex: 1, backgroundColor: '#fff', top: -1 }}>
          <Image square style={styles.drawerCover} source={drawerImage} />
          <List
            dataArray={datas}
            renderRow={data =>
              <ListItem
                button
                noBorder
                onPress={() => {
                  Actions.refresh({ key: 'drawer', open: value => !value });
                  Actions[data.route]();
                  this.props.closeDrawer();
                }}
              >
                <Left>
                  <Icon
                    active
                    name={data.icon}
                    style={{ color: '#777', fontSize: 26, width: 30 }}
                  />
                  <Text style={styles.text}>{data.name}</Text>
                </Left>
              </ListItem>}
          />

        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
export default connect(mapStateToProps, { closeDrawer })(SideMenu);
