import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Text,
  Body,
  Right,
  Card,
  CardItem,
  Icon
} from 'native-base';
import GoodItem from './commons/GoodItem';
import { MainHeader } from './commons';

class GoodsPromoView extends Component {

  render() {
    return (
      <Container>
        <MainHeader
          cartLength={this.props.goodChoice}
        />
        <Content>
          <Card>
            <CardItem>
              <Body>
                <Text>Promo</Text>
              </Body>
              <Right>
                <Icon
                  active
                  style={{ color: '#000', fontSize: 30 }}
                  name="md-arrow-dropdown"
                />
              </Right>
            </CardItem>
          </Card>
          <Body>
            <FlatList
              contentContainerStyle={styles.contaier}
              data={this.props.goodsList[0].goods}
              renderItem={({ item }) => <GoodItem {...item} />}
              keyExtractor={item => item.md5}
            />
          </Body>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    goodsList: state.goodsList,
    countGood: state.goodCart.countGood,
    goodChoice: state.goodCart.goodChoice.length
  };
};

const styles = {
  contaier: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  textStyle: {
    fontSize: 13,
    fontWeight: 'bold',
    color: '#fff',
    position: 'absolute',
    top: 8,
    left: 27,
    right: 0,
  }
};

export default connect(mapStateToProps)(GoodsPromoView);
