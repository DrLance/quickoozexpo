import React from 'react';
import { Body, Button, Text } from 'native-base';

const Footer = props =>
  <Footer style={{ backgroundColor: '#fff' }}>
    <Body>
      <Button full warning>
        <Text style={{ alignSelf: 'center' }}>Warning</Text>
      </Button>
    </Body>
  </Footer>;

export default Footer;
