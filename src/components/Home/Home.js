import React, { Component } from 'react';
import { Image, Linking } from 'react-native';
import { Input, Button, Form, Item, Label, Container, Content, Body, Text } from 'native-base';
import { connect } from 'react-redux';
import { loginUser } from '../../actions';
import images from '../../config/images';

class Home extends Component {
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf')
    });
  }
  onOpenURL() {
    Linking.openURL('http://google.com');
  }
  onOpenSignUp() {
    Linking.openURL('http://www.quickooz.com/signup');
  }

  onLoginPress() {
    const { email, password } = this.props;

    this.props.loginUser({ email, password });
  }

  render() {
    const { thumbnailStyle, textStyle, loginStyle, btnUrlStyle, lineStyle, textStyleSign } = styles;
    return (
      <Container>
        <Content padder>
          <Body>
            <Image style={thumbnailStyle} source={images.logo} />
          </Body>
          <Form>
            <Item floatingLabel last>
              <Label>Username</Label>
              <Input />
            </Item>
            <Item floatingLabel last>
              <Label>Password</Label>
              <Input secureTextEntry />
            </Item>
            <Button
              block
              style={{ margin: 15, marginTop: 50, backgroundColor: '#09a925' }}
              onPress={this.onLoginPress.bind(this)}
            >
              <Text style={loginStyle}>
                Login
              </Text>
            </Button>
          </Form>
          <Body>
            <Button transparent>
              <Text style={btnUrlStyle}>
                Fogot your login details?
              </Text>
            </Button>
          </Body>
          <Text style={lineStyle} />
          <Body>
            <Text style={textStyle}>
              Dont have an account?
            </Text>
            <Button transparent onPress={this.onOpenSignUp.bind(this)}>
              <Text style={textStyleSign}>
                Sign up here!
              </Text>
            </Button>
          </Body>
        </Content>
      </Container>
    );
  }
}

const styles = {
  thumbnailStyle: {
    height: 125,
    width: 125
  },
  textStyle: {
    paddingTop: 20,
    fontSize: 12
  },
  loginStyle: {
    fontFamily: 'Roboto'
  },
  btnUrlStyle: {
    paddingTop: 10,
    fontFamily: 'Roboto',
    fontSize: 10,
    textDecorationLine: 'underline',
    paddingBottom: 20
  },
  lineStyle: {
    paddingTop: 10,
    borderBottomWidth: 0.5,
    width: 350,
    fontFamily: 'Roboto'
  },
  textStyleSign: {
    paddingTop: 20,
    fontSize: 12,
    textDecorationLine: 'underline',
    paddingLeft: 5,
    fontFamily: 'Roboto'
  }
};

const mapStateToProps = ({ auth }) => {
  const { email, password } = auth;

  return { email, password };
};

export default connect(mapStateToProps, { loginUser })(Home);
