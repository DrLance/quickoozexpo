import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Content, Body, Input, Button, Text, Form, Item } from 'native-base';
import { connect } from 'react-redux';
import { searchPostalcode } from '../../actions';
import images from '../../config/images';

class RegionForm extends Component {
  onPressSearch() {
    const { postalcode } = this.props;

    this.props.searchPostalcode({ postalcode });
  }

  render() {
    return (
      <Container>
        <Content padder>
          <Body>
            <Image style={styles.thumbnailStyle} source={images.logo} />
          </Body>
          <Form>
            <Item floatingLabel last>
              <Input placeholder="Enter Postcode e.g Hp11" />
            </Item>
            <Button
              onPress={this.onPressSearch.bind(this)}
              block
              style={{ margin: 15, marginTop: 50, backgroundColor: '#09a925' }}
            >
              <Text>
                Search
              </Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({ searchPostal }) => {
  const { postalcode } = searchPostal;

  return { postalcode };
};

const styles = {
  thumbnailStyle: {
    height: 125,
    width: 125
  }
};

export default connect(mapStateToProps, { searchPostalcode })(RegionForm);
