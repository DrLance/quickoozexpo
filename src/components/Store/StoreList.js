import React, { Component } from 'react';
import { ListView } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Container, Content, Text, Body, Left, Header, Button, Icon as IconNB } from 'native-base';
import StoreRow from './StoreRow';
import { openDrawer } from '../../actions/DrawerActions';

class StoreList extends Component {
  componentWillMount() {
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.dataSource = ds.cloneWithRows(this.props.storeList);
  }

  onPressMenu() {
    this.props.openDrawer();
    //Actions.refresh({ key: 'drawer', open: value => !value });
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#fff' }}>
          <Left>
            <Button transparent onPress={this.onPressMenu.bind(this)}>
              <IconNB style={{ color: '#000' }} name="md-menu" />
            </Button>
          </Left>
          <Body>
            <Text>{this.props.title}</Text>
          </Body>
        </Header>
        <Content>
          <ListView
            contentContainerStyle={styles.contaier}
            dataSource={this.dataSource}
            renderRow={data => <StoreRow {...data} />}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return { storeList: state.storeList };
};

const styles = {
  contaier: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around'
  }
};

export default connect(mapStateToProps, { openDrawer })(StoreList);
