import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';

const styles = () => {
  const backButtonFunction = () => {
    return (
      <TouchableOpacity
        style={[{
          height: 27,
          width: ('100%'),
          flexDirection: 'row',
          alignItems: 'center',
        }]}
        onPress={() => { Actions.drawer({ type: 'BackAction' }); }}
      >
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Icon
            name="clear"
            size={25}
            color={'#000'}
          />
          <Text>Cancel</Text>
        </View>
      </TouchableOpacity>
    );
  };

  const rightButtonFunction = () => {
    return (
      <TouchableOpacity
        style={[{
          height: 25,
          width: ('100%'),
          flexDirection: 'row',
          alignItems: 'center',
        }]}
        onPress={() => { Actions.drawer(); }}
      >
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Icon
            name="check"
            size={25}
            color={'#000'}
          />
          <Text>Save</Text>
        </View>
      </TouchableOpacity>
    );
  };
}

export default styles;