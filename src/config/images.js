const images = {
  logo: require('../img/logo.png'),
  logoMenu: require('../img/logo_menu.png'),
  icons: {
    shopCart: require('../img/shopping-cart.png'),
    loginIcon: require('../img/login_icon.png')
  },
  testImg: require('../img/test_img.png'),
  listImg: require('../img/list.png'),
  testStore: require('../img/test_store.png')
};

export default images;
