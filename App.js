import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Expo from 'expo';
import configureStore from './src/config/store';
import Router from './src/Router';

class App extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      isReady: false,
      store: configureStore(() => this.setState({ isLoading: false }))
    };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Ionicons: require('native-base/Fonts/Ionicons.ttf')
    });

    this.setState({ isReady: true });
  }

  render() {
    return (
      <Provider store={this.state.store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
